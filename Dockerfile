FROM golang:alpine AS builder
LABEL maintainer="imlonghao <dockerfile@esd.cc>"
WORKDIR /go/src/app
COPY . ./
RUN apk add git && \
    go get -u github.com/golang/dep/cmd/dep && \
    dep ensure -vendor-only && \
    CGO_ENABLED=0 go build -o /app

FROM alpine
COPY --from=builder /app ./
RUN apk add ca-certificates
EXPOSE 8080
ENTRYPOINT [ "/app" ]