package main

import (
	"fmt"
	"regexp"
	"time"

	"github.com/gin-gonic/gin"
)

func webhookHandler(c *gin.Context, serviceID string) {
	if validator(c, serviceID) {
		go func() {
			fmt.Println(serviceID + ": Starting re-deploy service")
			for ranc.status(serviceID) != "active" {
				fmt.Println(serviceID + ": Other job is running")
				time.Sleep(time.Second * 1)
			}
			if ranc.upgrade(serviceID) == false {
				fmt.Println(serviceID + ": Error in service upgrade")
			}
			for ranc.status(serviceID) != "upgraded" {
				fmt.Println(serviceID + ": Upgrade in process")
				time.Sleep(time.Second * 1)
			}
			if ranc.finishUpgrade(serviceID) == false {
				fmt.Println(serviceID + ": Error in service finish upgrade")
			}
			fmt.Println(serviceID + ": Re-deploy finished")
		}()
		c.JSON(202, gin.H{"status": 202})
	} else {
		c.JSON(403, gin.H{"status": 403})
	}
}

func catchAllHandler(c *gin.Context) {
	webhookRule := regexp.MustCompile(`^/(\d+s\d+)$`)
	params := webhookRule.FindStringSubmatch(c.Request.URL.Path)
	if len(params) == 2 {
		webhookHandler(c, params[1])
		return
	}
}
