package main

import (
	"os"

	"github.com/gin-gonic/gin"
)

var ranc rancher

func main() {
	ranc = rancher{os.Getenv("R-BASEURL"), os.Getenv("R-ACCESS-KEY"), os.Getenv("R-SECRET-KEY")}
	web := gin.Default()
	web.NoRoute(catchAllHandler)
	web.Run()
}
