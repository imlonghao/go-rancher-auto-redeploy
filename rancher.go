package main

import (
	"bytes"
	"net/http"

	"github.com/json-iterator/go"
)

type rancher struct {
	baseURL   string
	accessKey string
	secretKey string
}

func (r rancher) status(serviceID string) string {
	client := http.Client{}
	req, _ := http.NewRequest("GET", r.baseURL+serviceID, nil)
	req.SetBasicAuth(r.accessKey, r.secretKey)
	res, _ := client.Do(req)
	buffer := new(bytes.Buffer)
	buffer.ReadFrom(res.Body)
	return jsoniter.Get(buffer.Bytes(), "state").ToString()
}

func (r rancher) upgrade(serviceID string) bool {
	client := http.Client{}
	req, _ := http.NewRequest("GET", r.baseURL+serviceID, nil)
	req.SetBasicAuth(r.accessKey, r.secretKey)
	res, _ := client.Do(req)
	buffer := new(bytes.Buffer)
	buffer.ReadFrom(res.Body)
	launchConfig := jsoniter.Get(buffer.Bytes(), "launchConfig")
	secondaryLaunchConfigs := jsoniter.Get(buffer.Bytes(), "secondaryLaunchConfigs")
	type uploadData struct {
		InServiceStrategy struct {
			BatchSize              int          `json:"batchSize"`
			IntervalMillis         int          `json:"intervalMillis"`
			LaunchConfig           jsoniter.Any `json:"launchConfig"`
			SecondaryLaunchConfigs jsoniter.Any `json:"secondaryLaunchConfigs"`
			StartFirst             bool         `json:"startFirst"`
		} `json:"inServiceStrategy"`
	}
	ud := new(uploadData)
	ud.InServiceStrategy.BatchSize = 1
	ud.InServiceStrategy.IntervalMillis = 2000
	ud.InServiceStrategy.LaunchConfig = launchConfig
	ud.InServiceStrategy.SecondaryLaunchConfigs = secondaryLaunchConfigs
	ud.InServiceStrategy.StartFirst = false
	udjson, _ := jsoniter.Marshal(&ud)
	req, _ = http.NewRequest("POST", r.baseURL+serviceID+"/?action=upgrade", bytes.NewBuffer(udjson))
	req.SetBasicAuth(r.accessKey, r.secretKey)
	req.Header.Set("Content-Type", "application/json")
	res, _ = client.Do(req)
	return res.StatusCode == 202
}

func (r rancher) finishUpgrade(serviceID string) bool {
	client := http.Client{}
	req, _ := http.NewRequest("POST", r.baseURL+serviceID+"/?action=finishupgrade", bytes.NewBufferString("{}"))
	req.SetBasicAuth(r.accessKey, r.secretKey)
	req.Header.Set("Content-Type", "application/json")
	res, _ := client.Do(req)
	return res.StatusCode == 202
}
