package main

import (
	"io/ioutil"

	"github.com/gin-gonic/gin"
	"gopkg.in/yaml.v2"
)

type validatorConfig struct {
	Type  string `yaml:"type"`
	Token string `yaml:"token"`
}

func validator(c *gin.Context, serviceID string) bool {
	file, _ := ioutil.ReadFile("./data/config.yml")
	vConfig := make(map[string]validatorConfig)
	_ = yaml.Unmarshal(file, &vConfig)
	switch vConfig[serviceID].Type {
	case "gitlab":
		if gitlabValidator(c, vConfig[serviceID].Token) {
			return true
		}
	case "dockerhub":
		if dockerhubValidator(c, vConfig[serviceID].Token) {
			return true
		}
	}
	return false
}

func gitlabValidator(c *gin.Context, token string) bool {
	type gitlabResponse struct {
		ObjectAttributes struct {
			Status string `json:"status" binding:"required"`
		} `json:"object_attributes" binding:"required"`
	}
	var gitlabRes gitlabResponse
	c.BindJSON(&gitlabRes)
	if c.Request.Header.Get("X-Gitlab-Event") != "Pipeline Hook" {
		return false
	}
	if c.Request.Header.Get("X-Gitlab-Token") != token {
		return false
	}
	if gitlabRes.ObjectAttributes.Status != "success" {
		return false
	}
	return true
}

func dockerhubValidator(c *gin.Context, token string) bool {
	if c.Query("token") != token {
		return false
	}
	return true
}
